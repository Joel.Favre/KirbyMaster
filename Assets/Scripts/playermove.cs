﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class playermove : MonoBehaviour {

public Rigidbody rb;
public float ForwardForce = 20f;
public float SidewaysForce = 60f;
public bool isMoving = true;

    // Start is called before the first frame update
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }
    /*void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            isMoving = !isMoving;
        }
    }*/ //To pause the player
    // Update is called once per frame
    void FixedUpdate()
{
    // Check if the player should be moving
    if (isMoving)
    {
        // Add a forward force
        rb.AddForce(0, 0, ForwardForce * Time.deltaTime, ForceMode.VelocityChange);

        // Wait for an input
        if (Input.GetKey("d") || Input.GetKey("right") || Input.GetMouseButton(1))
        {
            rb.AddForce(SidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }
        else if (Input.GetKey("a") || Input.GetKey("left") || Input.GetMouseButton(0))
        {
            rb.AddForce(-SidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.position.x < 900)
            {
                rb.AddForce(-SidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
            }
            else if (touch.position.x > 1000)
            {
                rb.AddForce(SidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
            }
            Debug.Log(touch.position);
        }
    }

    if (rb.position.y < 0f)
    {
        FindFirstObjectByType<GameManager>().EndGame();
    }
}

}