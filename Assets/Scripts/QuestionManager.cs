using UnityEngine;
using TMPro;
using System.Linq;

public class QuestionManager : MonoBehaviour
{
    public TextMeshProUGUI questionText;  // Referencia al componente TextMeshProUGUI para la pregunta
    public TextMeshProUGUI answer1Text; 
    public TextMeshPro answer1BText; // Referencia al componente TextMeshProUGUI para la primera respuesta
    public TextMeshProUGUI answer2Text; 
    public TextMeshPro answer2BText; // Referencia al componente TextMeshProUGUI para la segunda respuesta
    public GameObject leftGroup;   // Reference to the empty parent GameObject for the left group
    public GameObject rightGroup;  // Reference to the empty parent GameObject for the right group
    public string bonneréponse;
    private string[] easyQuestions = {
    "4 + 6", "8 - 3", "5 + 2", "9 - 5", "3 + 7","10 - 2", "6 + 4", "12 - 8", "2 + 8", "7 - 1","9 + 1", 
    "4 + 3", "8 - 2", "6 + 5", "10 - 4","3 + 6", "7 - 4", "5 + 4", "12 - 3", "2 + 5","8 - 1", "9 + 3",
     "6 + 2", "10 - 1", "4 + 1","7 - 3", "5 + 3", "12 - 6", "10 - 3", "9 + 2"
    };  // list of questions easy 
    private string[] easyAnswers = {"10", "5", "7", "4", "10","8", "10", "4", "10", "6","10", "7", "6",
    "11", "6","9", "3", "9", "9", "7","7", "12", "8", "9", "5","4", "8", "6", "7", "11"};  // List of answers to easy questions

    private string[] mediumQuestions = {
    "8 × 3", "15 ÷ 5", "4^2", "6 + 7", "9 - 3","5 × 4", "12 ÷ 3", "3^2", "10 + 4", "18 - 6",
    "7 × 2", "16 ÷ 4", "5^2", "9 - 5", "8 + 3","14 - 6", "6 × 3", "10 ÷ 2", "4 + 3", "12 - 8",
    "9 × 2", "16 ÷ 8", "7^2", "15 - 7", "10 + 5","8 × 2", "18 ÷ 6", "3^3", "6 × 4", "14 - 9"}; 

    private string[] mediumAnswers = {
    "24", "3", "16", "13", "6","20", "4", "9", "14", "12","14", "4", "25", "4", "11",
    "8", "18", "5", "7", "4","18", "2", "49", "8", "15","16", "3", "27", "24", "5"};


    private string[] hardQuestions = {
    "12 × 3", "16 ÷ 4", "5^2", "3 × (4 + 2)", "25 ÷ 5",
    "8^2", "2 × 6 + 4", "(7 × 2) - 5", "15 ÷ 3", "9 × 4 - 3",
    "20 ÷ 4 + 2", "(10 - 3) × 2", "6^2 - 4", "18 ÷ 2 + 5", "11 × 3 + 4",
    "(9 + 6) ÷ 3", "4^2", "24 ÷ (5 + 3)", "8 × 3 - 2", "16 ÷ (4 × 2)",
    "(12 - 4) × 2", "7^2", "14 ÷ 2 + 3", "18 - (3 × 2)", "3 × (5 + 7)",
    "(9 + 3) ÷ 4", "10^2", "24 ÷ 4 + 2", "(6 × 2) - 3", "(15 - 6) × 2"}; 

private string[] hardAnswers = {
    "36", "4", "25", "18", "5","64", "16", "9", "5", "33","7", "14", "32", "14", "37",
    "5", "16", "3", "22", "2","16", "49", "10", "12", "36","3", "100", "8", "9", "18"};


    private int currentQuestionIndex = 0;  // Index of the current question

    void Start()
    {
        // Shuffle the questions and answers together
        System.Random rng = new System.Random();
        var easyQA = easyQuestions.Zip(easyAnswers, (q, a) => new { Question = q, Answer = a })
            .OrderBy(x => rng.Next())
            .ToArray();
        easyQuestions = easyQA.Select(x => x.Question).ToArray();
        easyAnswers = easyQA.Select(x => x.Answer).ToArray();

        var mediumQA = mediumQuestions.Zip(mediumAnswers, (q, a) => new { Question = q, Answer = a })
            .OrderBy(x => rng.Next())
            .ToArray();
        mediumQuestions = mediumQA.Select(x => x.Question).ToArray();
        mediumAnswers = mediumQA.Select(x => x.Answer).ToArray();

        var hardQA = hardQuestions.Zip(hardAnswers, (q, a) => new { Question = q, Answer = a })
            .OrderBy(x => rng.Next())
            .ToArray();
        hardQuestions = hardQA.Select(x => x.Question).ToArray();
        hardAnswers = hardQA.Select(x => x.Answer).ToArray();

        ShowNextQuestion();
    }

    public void ShowNextQuestion()
    {
        Vector3 currentPosition = answer1BText.transform.position;
        Vector3 currentPosition2 = answer2BText.transform.position;
        // Offset the position by 50 on the Z-axis
        Vector3 offsetPosition = new Vector3(currentPosition.x, currentPosition.y, currentPosition.z + 50f);
        Vector3 offsetPosition2 = new Vector3(currentPosition2.x, currentPosition2.y, currentPosition2.z + 50f);

        // Apply the new position to the TextMeshPro object
        answer1BText.transform.position = offsetPosition;
        answer2BText.transform.position = offsetPosition2;
        // Select the list of questions and answers based on the player's score
        string[] questions;
        string[] answers;
        if (Score.Instance.score < 4)
        {
            questions = easyQuestions;
            answers = easyAnswers;
            //Debug.Log("Easy difficulty");
        }
        else if (Score.Instance.score >= 5)
        {
            questions = hardQuestions;
            answers = hardAnswers;
            //Debug.Log("Hard difficulty");
        }
        else 
        {
            questions = mediumQuestions;
            answers = mediumAnswers;
            //Debug.Log("Medium difficulty");
        }
        if (currentQuestionIndex < questions.Length)
        {
            questionText.text = questions[currentQuestionIndex];

            // Generate a random number for the incorrect answer
            int correctAnswer = int.Parse(answers[currentQuestionIndex]);
            int incorrectAnswer = UnityEngine.Random.Range(int.Parse(answers[currentQuestionIndex])-5, int.Parse(answers[currentQuestionIndex])+5);
            
            // Make sure the incorrect answer is different from the correct answer
            while (incorrectAnswer == correctAnswer)
            {
                incorrectAnswer = UnityEngine.Random.Range(0, 100);
            }

            // Randomly assign the correct and incorrect answers to the left and right options
            if (UnityEngine.Random.value > 0.5f)
            {
                answer1Text.text = answers[currentQuestionIndex]; //Left answer
                answer1BText.text = answers[currentQuestionIndex]; //Left answer
                answer2Text.text = incorrectAnswer.ToString();
                answer2BText.text = incorrectAnswer.ToString();  //Right answer
                bonneréponse =  questions[currentQuestionIndex] + " = "+answers[currentQuestionIndex];
                //Debug.Log("Answer on the left");
                UpdateTagsInGroup(leftGroup, "Right_answer"); //Changement des tags des objets pour le comptage des points
                UpdateTagsInGroup(rightGroup, "Wrong_answer");
            }
            else
            {
                answer1Text.text = incorrectAnswer.ToString();
                answer1BText.text = incorrectAnswer.ToString();
                answer2Text.text = answers[currentQuestionIndex];
                answer2BText.text = answers[currentQuestionIndex];
                bonneréponse =  questions[currentQuestionIndex] + " = "+answers[currentQuestionIndex];
                //Debug.Log("Answer on the right");
                UpdateTagsInGroup(leftGroup, "Wrong_answer");
                UpdateTagsInGroup(rightGroup, "Right_answer");
            }
        currentQuestionIndex++;
        }
        else
        {
            Debug.Log("No more questions.");
        }
    }
    void UpdateTagsInGroup(GameObject group, string tag) //Module de mise à jour des tags
    {
        // Check if the group GameObject is null
        if (group == null)
        {
            Debug.LogError("Group GameObject is not assigned.");
            return;
        }

        // Iterate through each child GameObject of the group
        foreach (Transform child in group.transform)
        {
            // Update the tag of the child GameObject
            child.gameObject.tag = tag;
        }
    }

    }


