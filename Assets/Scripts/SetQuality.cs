using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetQuality : MonoBehaviour
{
    [SerializeField] private Dropdown qualityDropdown;

    private void Start()
    {
        InitializeDropdown();
    }

    private void InitializeDropdown()
    {
        qualityDropdown.ClearOptions();
        string[] qualityLevels = QualitySettings.names;
        qualityDropdown.AddOptions(new List<string>(qualityLevels));
        qualityDropdown.value = GetCurrentQualityIndex();
        qualityDropdown.onValueChanged.AddListener(SetQualityLevelDropdown);
    }

    private int GetCurrentQualityIndex()
    {
        string currentQuality = QualitySettings.names[QualitySettings.GetQualityLevel()];
        return qualityDropdown.options.FindIndex(option => option.text == currentQuality);
    }

    public void SetQualityLevelDropdown(int index)
    {
        QualitySettings.SetQualityLevel(index, false);
    }

    private void OnDestroy()
    {
        qualityDropdown.onValueChanged.RemoveListener(SetQualityLevelDropdown);
    }
}
