﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class Score : MonoBehaviour
{
    public static Score Instance; // Singlenton instance
    
    public Transform player;
    public Text scoreText;
    public float score;  // score's rating system to determine the difficulty level
    public int score_global = 0;  // Score of the player
    private const float correctAnswerPoints = 0.25f;  // Points for each answer right
    private const float incorrectAnswerPoints = -0.25f;  // Points for each answer wrong 

    void Awake()
    {
        // Singleton pattern
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    
    // setup the score at 4.50 points
    void Start()
    {
        score = 4.5f;
    }
    // mis à jour
    void Update()
    {
        scoreText.text = score.ToString("0.00");
    }

    // call when the player answer right 
    public void CorrectAnswer()
    {
        if(score >=6){score = 6;}
        else{score += correctAnswerPoints;}
        Debug.Log("Score: " + score);
    }

    // call when the player answer wrong 
    public void IncorrectAnswer()
    {
        if(score <=0){score = 0;}
        else{score += incorrectAnswerPoints;}
        Debug.Log("Score: " + score);
    }
}