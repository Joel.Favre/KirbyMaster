﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{ 
public static int level_achieved = 0; // Declare as static

    public void RestartGame()
    {
        level_achieved = 0; // Reset the level_achieved variable
       
        SceneManager.LoadScene("Level 1");
        Score.Instance.score_global = 0;
        PlayerCollision.sauvegardequestions = "";
    }
public void QuitGame()
{
    Application.Quit();
}
public void Levels()
{ 
   SceneManager.LoadScene("Instructions");
}
public void Leaderboard()
{ 
   SceneManager.LoadScene("Leaderboard");
}
    //Activate button sound
    public void button()
    {
        FindFirstObjectByType<AudioManager>().Play("Menu Button");
    }
}

