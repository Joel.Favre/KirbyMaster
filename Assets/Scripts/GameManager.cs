﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    bool gameHasEnded = false;
    public float RestartDelay = 1f;
    public GameObject LevelCompleteUI;
    public Text levelnumb;
    public Text mainscore; // Reference to the UI Text displaying the mainscore count
    

    void Update()
    {
        // Update the mainscore text
        mainscore.text = "Score: " + Score.Instance.score_global.ToString();
    }

public void LevelCompleted()
    {
        levelnumb.text = (SceneManager.GetActiveScene().name);
        FindFirstObjectByType<AudioManager>().Play("Victory");
        LevelCompleteUI.SetActive(true);
    }


    public void CompleteLevel()
    {
        LevelCompleted();

    }
    public void EndGame()
    {
        if(gameHasEnded == false)
        {
            //Debug.Log("You Loose");
            gameHasEnded = true;
            Invoke("Restart", RestartDelay);
        }

    }
    private void FixedUpdate()
{
    if (Input.GetKey(KeyCode.Escape)) // Check if the Escape key is pressed
    {
        SceneManager.LoadScene("Start");
    }
}

    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

  
}
