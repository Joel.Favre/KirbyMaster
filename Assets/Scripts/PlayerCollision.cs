using UnityEngine;
using System.IO;
using System.Collections.Generic;
using TMPro;

public class PlayerCollision : MonoBehaviour
{
    public playermove Movement;
    public GameObject explosionfx;
    public Transform player;
    public MeshRenderer myplayer;
    public string playerUsername;
    private bool canIncrementDeath = true;
    private float lastDeathTime = 0f;
    private float deathIncrementInterval = 3f; // Interval in seconds
    private int bonus_enchainement = 0;
    private QuestionManager questionManager;
    private float delay_collision; //Délai de collision pour les réponses afin d'éviter une double collision du même objet
    public int randomNumber;
    public static string sauvegardequestions;
    public Dictionary<string, string> myDictionary = new Dictionary<string, string>();
    public TextMeshProUGUI lesmauvaisesréponses; 
    
    void Start(){
        // Get reference to the QuestionManager instance
        questionManager = FindFirstObjectByType<QuestionManager>();
        
    }
    void OnCollisionEnter(Collision collisionInfo)
    {
        if (canIncrementDeath && collisionInfo.collider.CompareTag("Obstacle"))
        {
            IncrementDeath();
        }
        if (collisionInfo.collider.CompareTag("Mur"))
        {
            int randomNumber = Random.Range(1, 5);
            if (randomNumber== 1){FindFirstObjectByType<AudioManager>().Play("Aie aie aie");}
            else if (randomNumber== 2){FindFirstObjectByType<AudioManager>().Play("Ouch");}
            else if (randomNumber== 3){FindFirstObjectByType<AudioManager>().Play("Dur pierre");}
            else if (randomNumber== 4){FindFirstObjectByType<AudioManager>().Play("Ma tête");}
        }
        if (Time.time - delay_collision > 2f)
        {
        if (collisionInfo.collider.CompareTag("Right_answer"))
        {
            int randomNumber = Random.Range(1, 5);
            if (randomNumber== 1){FindFirstObjectByType<AudioManager>().Play("Right answer1");}
            else if (randomNumber== 2){FindFirstObjectByType<AudioManager>().Play("Right answer2");}
            else if (randomNumber== 3){FindFirstObjectByType<AudioManager>().Play("Right answer3");}
            else if (randomNumber== 4){FindFirstObjectByType<AudioManager>().Play("Right answer4");}
            questionManager.ShowNextQuestion();
            //Debug.Log("Correct answer");
            //Debug.Log(randomNumber);
        if (Score.Instance.score < 4)
        {
            Score.Instance.score_global +=1;
            Score.Instance.CorrectAnswer();
            //Debug.Log("Easy difficulty, Your score is: "+ Score.Instance.score_global);
        }
        else if (Score.Instance.score >= 5)
        {
            if (bonus_enchainement >= 5){Score.Instance.score_global +=7;
            Score.Instance.CorrectAnswer();}
            else{Score.Instance.score_global +=5;
            Score.Instance.CorrectAnswer();
            bonus_enchainement++;}
            //Debug.Log("Hard difficulty, Your score is: "+ Score.Instance.score_global);
        }
        else 
        {
            Score.Instance.score_global +=3;
            Score.Instance.CorrectAnswer();
            //Debug.Log("Medium difficulty, Your score is: "+ Score.Instance.score_global);
        }delay_collision = Time.time;}
        if (Time.time - delay_collision > 2f){
        if (collisionInfo.collider.CompareTag("Wrong_answer"))
        {
            sauvegardequestions = sauvegardequestions + questionManager.bonneréponse+"\n";
            lesmauvaisesréponses.text = sauvegardequestions;
            myDictionary.Add(questionManager.bonneréponse, "");
            foreach (KeyValuePair<string, string> kvp in myDictionary)
        {
            Debug.Log("Key: " + kvp.Key);
            Debug.Log(sauvegardequestions);
            //Debug.Log("Length is "+ myDictionary.Count);
        }

            int randomNumber = Random.Range(1, 4);
            if (randomNumber== 1){FindFirstObjectByType<AudioManager>().Play("Wrong answer1");}
            else if (randomNumber== 2){FindFirstObjectByType<AudioManager>().Play("Wrong answer2");}
            else if (randomNumber== 3){FindFirstObjectByType<AudioManager>().Play("Wrong answer3");}
            Score.Instance.IncorrectAnswer();
            questionManager.ShowNextQuestion();
            bonus_enchainement = 0; //On reset le bonus d'enchainement dès qu'une question est fausse
            Debug.Log("Wrong answer");
            delay_collision = Time.time;
        }
        }}
    }
    private void OnParticleCollision(GameObject other)
    {
        if (canIncrementDeath)
        {
            IncrementDeath();
        }
    }

void IncrementDeath()
{
    Movement.enabled = false;
    FindFirstObjectByType<AudioManager>().Play("Explosion");
    Instantiate(explosionfx, transform.position, transform.rotation);
    Destroy(myplayer);
    FindFirstObjectByType<GameManager>().EndGame();
    canIncrementDeath = false;
    lastDeathTime = Time.time;
    Invoke("ResetDeathIncrement", deathIncrementInterval);
}


    void ResetDeathIncrement()
    {
        canIncrementDeath = true;
    }
}
